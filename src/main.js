import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import QuizQuestions from "./views/QuizQuestions"
import StartPage from "./views/StartPage"

Vue.use(VueRouter);

Vue.config.productionTip = false

// Setting up routes
const routes = [
  {
    path: "/",
    name:"Start quiz",
    component: StartPage
  },
  {
    path: "/quiz",
    name:"Quiz",
    component: QuizQuestions
  }
];


const router = new VueRouter({
  mode: "history",
  routes: routes
})


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
